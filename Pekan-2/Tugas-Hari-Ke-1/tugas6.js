// soal 1
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]

// jawaban soal 1
var arrayDaftarPeserta = {
    nama: "Asep",
    jenisKelamin: "laki-laki",
    hobi: "baca buku",
    tahunLahir: 1992
}
console.log(arrayDaftarPeserta.nama);
// soal 1

// soal 2
// 1.  nama: strawberry
//     warna: merah
//     ada bijinya: tidak
//     harga: 9000
// 2.  nama: jeruk
//     warna: oranye
//     ada bijinya: ada
//     harga: 8000
// 3.  nama: Semangka
//     warna: Hijau & Merah
//     ada bijinya: ada
//     harga: 10000
// 4.  nama: Pisang
//     warna: Kuning
//     ada bijinya: tidak
//     harga: 5000

// jawaban 2
var buah = [{name: "strawberry", warna: "merah", biji: "tidak", harga: 9000}, {
    name: "jeruk",
    warna: "oranye",
    biji: "ada",
    harga: 8000
}, {name: "Semangka", warna: "Hijau & Merah", biji: "ada", harga: 10000}, {
    name: "Pisang",
    warna: "Kuning",
    biji: "tidak",
    harga: 5000
},]

console.log(buah[0])
// soal 2

// soal 3
var dataFilm = [];

// jawaban 3
var addFilm = {
    nama: "dilan",
    durasi: "2 jam",
    genre: "romance",
    tahun: 2019
}

function addDataFilm() {
    dataFilm.push(addFilm);
    return console.log(dataFilm);
}

addDataFilm();
// jawaban 3
// soal 3

// soal 4
class Animal {
    // jawaban 4 "Release 0"
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }

    // jawaban 4 "Release 0"
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//jawaban 4 "Release 1"
class Ape extends Animal {
    constructor(name) {
        super(name);
    }

    yell() {
        return console.log(this.yell = "Auooo");
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        return console.log(this.jump = "hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
//jawaban 4 "Release 1"
// soal 4

//soal 5
// function Clock({ template }) {
//
//     var timer;
//
//     function render() {
//         var date = new Date();
//
//         var hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;
//
//         var mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;
//
//         var secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;
//
//         var output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs);
//
//         console.log(output);
//     }
//
//     this.stop = function() {
//         clearInterval(timer);
//     };
//
//     this.start = function() {
//         render();
//         timer = setInterval(render, 1000);
//     };
//
// }
//
// var clock = new Clock({template: 'h:m:s'});
// clock.start();

class Clock {
    // Code di sini
    // jawaban 5
    constructor({template}) {
        var timer;

        function render() {
            var date = new Date();

            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;

            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;

            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;

            var output = template
                .replace('h', hours)
                .replace('m', mins)
                .replace('s', secs);

            console.log(output);
        }

        this.stop = function () {
            clearInterval(timer);
        };

        this.start = function () {
            render();
            timer = setInterval(render, 1000);
        };

    }
    // jawaban 5
}

var clock = new Clock({template: 'h:m:s'});
clock.start();
//soal 5