// soal 1
// jawaban 1
let hasilLuas = "";
let hasilKeliling = "";

const luasLingkaran = (r) => hasilLuas = 3.14 * (r * r);
const kelilingLingkaran = (d) => hasilKeliling = 3.14 * (d);

console.log(luasLingkaran(10))
console.log(kelilingLingkaran(10))
// soal 1

// soal 2
let kalimat = ""
// jawaban 2
const tambahKalimat = (saya, adalah, seorang, frontend, developer) => {
    return kalimat = `${saya} ${adalah} ${seorang} ${frontend} ${developer}`
}
console.log(tambahKalimat('saya', 'adalah', 'seorang', 'frontend', 'developer'));
// soal 2

//soal 3
// jawaban 3
class Book {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }
    present() {
        return 'Nama buku ini ' + this.name + ' total halamannya ' + this.totalPage + ' harga buku ini ' + this.price;
    }
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price);
        this.isColorful = isColorful;
    }
    show() {
        if (this.isColorful == false) {
            return this.present() + ', dan buku ini tidak bewarna';
        } else {
            return this.present() + ', dan buku ini bewarna';
        }
    }
}

const myBook = new Komik("LOTR", 1, 1000, false);
console.log(myBook.show());
// soal 3