import React from 'react';
import './App.css';

function App() {
    return (
        <div className="App">
            <div className={"form-pembelian"}>
                <h1>Form Pembelian Buah</h1>
                <label>
                    <span>
                    Nama Pelanggan
                    </span>
                    <input type="text" name="name"/>
                </label>
                <div className={"wrapper-form"}>
                    <div className={"title-daftar"}>
                        Daftar Item
                    </div>
                    <div>
                        <label>
                            <input type="checkbox" name="name" value={"semangka"}/>
                            Semangka
                        </label><br/>
                        <label>
                            <input type="checkbox" name="name" value={"jeruk"}/>
                            Jeruk
                        </label><br/>
                        <label>
                            <input type="checkbox" name="name" value={"nanas"}/>
                            Nanas
                        </label><br/>
                        <label>
                            <input type="checkbox" name="name" value={"salak"}/>
                            Salak
                        </label><br/>
                        <label>
                            <input type="checkbox" name="name" value={"anggur"}/>
                            Anggur
                        </label>
                    </div>
                </div>
                <button>Kirim</button>
            </div>
        </div>
    );
}

export default App;
