// soal 1 (literals)
const newFunction = function literal(firstName, lastName){
    return {
        //jawaban 1
        firstName,
        lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
            return
        }
    }
}

//Driver Code
newFunction("William", "Imoh").fullName()
//soal 1

//soal 2 (Destructuring)
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
//jawaban 2
const {firstName, lastName, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation);
//soal 2

//soal 3 (array spreading)
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

//jawaban 3
const combined = [...west, ...east];

//Driver Code
console.log(combined)
//soal 3