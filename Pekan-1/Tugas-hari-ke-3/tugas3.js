// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

// jabawan soal 1
console.log(kataPertama + ' ' + kataKedua + ' ' + kataKetiga + ' ' + kataKeempat);

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

// jawaban soal 2
var satu = parseInt(kataPertama);
var dua = parseInt(kataKedua);
var tiga = parseInt(kataKetiga);
var empat = parseInt(kataKeempat);
console.log(satu + dua + tiga + empat);

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua; // do your own!
var kataKetiga; // do your own!
var kataKeempat; // do your own!
var kataKelima; // do your own!


// jawaban soal 3
var kataKedua = kalimat.substring(4, 14); // do your own!
var kataKetiga = kalimat.substring(15, 19); // do your own!
var kataKeempat = kalimat.substring(19, 24); // do your own!
var kataKelima = kalimat.substring(25, 31); // do your own!

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

// soal 4

// var nilai;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E

// jabawan soal 4
var nilai = 81;

if (nilai >= 80) {
    console.log('A')
} else if (nilai >= 70 && 80) {
    console.log('B')
} else if (nilai >= 60 && 70) {
    console.log('C')
} else if (nilai >= 50 && 60) {
    console.log('D')
} else {
    console.log('E')
}

// jawaban soal 5
var tanggal = 11;
var bulan = 3;
var tahun = 1996;
switch (bulan) {
    case 1: {
        bulan = "Januari"
        break;
    }
    case 2: {
        bulan = "Februari"
        break;
    }
    case 3: {
        bulan = "Maret"
        break;
    }
    case 4: {
        bulan = "April"
        break;
    }
    case 5: {
        bulan = "Mei"
        break;
    }
    case 6: {
        bulan = "Juni"
        break;
    }
    case 7: {
        bulan = "Juli"
        break;
    }
    case 8: {
        bulan = "Agustus"
        break;
    }
    case 9: {
        bulan = "September"
        break;
    }
    case 10: {
        bulan = "Oktober"
        break;
    }
    case 11: {
        bulan = "November"
        break;

    }
    case 12: {
        bulan = "Desember"
        break;
    }
}
console.log(tanggal + " " + bulan + " " + tahun)