// jawaban soal 1
function halo() { // jawaban function
  return  'Halo Sanbers!';
} // jawaban function

console.log(halo());
// jawaban soal 1

console.log('\n')

// jabawan soal 2
function kalikan(num1, num2) { // jawaban function
    return num1 * num2
} // jawaban function

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
// jabawan soal 2

console.log('\n')

// jawaban soal 3
function introduce(name, age, address, hobby) { // jawaban function
    return 'Nama saya ' + name + ', umur saya ' + age + ' tahun, ' + 'alamat saya di ' + address + ', dan saya punya hobby yaitu ' + hobby + '!'
} // jawaban function

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"
// jawaban soal 3

